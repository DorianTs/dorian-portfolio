#!/bin/bash

# Logging in to AWS ECR 
aws ecr-public get-login-password --region us-east-1 | docker login --username AWS --password-stdin public.ecr.aws/k9s6i6f6

# Pulling the latest version of my application
docker pull public.ecr.aws/k9s6i6f6/dorianportfolio:latest