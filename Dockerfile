FROM public.ecr.aws/k9s6i6f6/nginx:25.0
COPY ./assets /usr/share/nginx/html/assets/
COPY ./index.html /usr/share/nginx/html/index.html
